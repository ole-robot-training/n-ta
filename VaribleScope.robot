*** Settings ***
Suite Setup       Prepare Suite Setup

*** Variables ***
${Lang}           EN
@{ProductList}    book    Pen
&{Profile}        id=1    name=som
@{Tools}          Pen    Ruler
@{Car}            Wheel    Engine

*** Test Cases ***
01
    Log    ${Lang}
    Log    ${ProductList}
    Log    ${Profile}

02
    ${result}    Get Product    1

03
    ${result}    Get Product By Index    0

04
    ${result}    Get Products    book

05
    ${result}    Get Product By Name    Tools
    Log    ${result}
    ${result}    Get Product By Name 01    Car
    Log    ${result}

06
        Get Data    A
    Log    ${result}

07
    Log    ${Data01}

*** Keywords ***
Get Product
    [Arguments]    ${id}
    [Return]    Book

Get Product By Index
    [Arguments]    ${index}
    Return from keyword    ${ProductList}[${index}]

Get Products
    [Arguments]    ${name}
    Return From Keyword    ${ProductList}

Get Product By Name
    [Arguments]    ${name}
    Log    ${name}    #Tools
    Log    ${Tools}
    Log    ${${name}}
    Return From Keyword    ${${name}}

Get Product By Name 01
    [Arguments]    ${name}
    Log    ${name}    #Tools
    Log    ${Tools}
    Log    ${${name}}
    ${result}    Set Variable    ${${name}}
    [Return]    ${result}

Get Data
    [Arguments]    ${name}
    ${result}    Set Variable    helloworld
    Set Test Variable    ${result}
    Comment    Set Test Variable    ${result}    helloworld

Prepare Suite Setup
    ${Data01}    Set Variable    01
    Set Suite Variable    ${Data01}
