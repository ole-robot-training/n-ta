*** Settings ***
Documentation     Account all
Suite Setup       Log    M
Suite Teardown    Log    N
Test Setup        Log    O
Test Teardown     Log    P
Test Timeout      20 seconds
Library           Collections
Variables         data.yaml

*** Test Cases ***
01
    [Documentation]    Helloworld
    [Tags]    Account
    [Setup]    Log    A
    [Timeout]    10 seconds
    Log    helloworld
    # Sleep    11
    [Teardown]    Log    B

02
    Log    helloworld
    Sleep    10s
